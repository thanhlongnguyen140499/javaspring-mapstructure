package com.example.mapstructure.springmapstructuredemo.mapper;

import com.example.mapstructure.springmapstructuredemo.dto.ProductDto;
import com.example.mapstructure.springmapstructuredemo.model.Product;

public interface ProductMapper {

    ProductDto modelToDto(Product product);
    Product dtoToModel(ProductDto productDto);

}
