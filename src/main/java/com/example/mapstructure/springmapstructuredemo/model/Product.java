package com.example.mapstructure.springmapstructuredemo.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table
public class Product {

    @Id
    private int id;
    private String name;
    private String desc;
    private int  quantity;
    private long price;
}
