package com.example.mapstructure.springmapstructuredemo.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table
public class ProductDto {

    @Id
    private int id;
    private String name;
    private int  quantity;
    private long price;
}
